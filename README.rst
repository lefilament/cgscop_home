.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


========================
CG Scop - Page d'Accueil
========================

Description
===========

Ce module ajoute un écran d'accueil pour la CG Scop.
Il permet d'afficher les éléments suivants : 

* calendrier
* activités
* liens
* heures imputées


Dependencies
------------

Ce module utilise les librairies fullcalendar (please visit https://fullcalendar.io/ for docs and licenses)


Credits
=======

Funders
-------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Benjamin Rivier <benjamin@le-filament.com>
* Remi Cazenave <remi@le-filament.com>
* Juliana Poudou <juliana@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament