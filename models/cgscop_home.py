# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import timedelta
from dateutil.relativedelta import relativedelta

from odoo import models, api, fields
from odoo.fields import Date
from odoo.tools import float_round


class ScopHome(models.AbstractModel):
    _name = "cgscop.home"
    _description = "Page accueil CG Scop"
    _order = "date_tdb desc"

    @api.model
    def get_home_values(self):
        # Variables
        partner = self.env.user.partner_id
        week_start = Date.today()+relativedelta(weeks=-1, days=1, weekday=0)
        week_end = Date.today()+relativedelta(weeks=0, weekday=6)
        month_start = Date.today()+relativedelta(months=-1, day=1)
        month_end = Date.today()+relativedelta(day=1, days=-1)

        # Agenda de l'utilisateur
        event_ids = self.env['calendar.event'].sudo().search([
            ['partner_ids', 'in', partner.id],
            ['scop_resource_id', '=', False]])
        events = list(map(lambda e: {
            'title': e.name,
            'start': fields.Datetime.context_timestamp(
                self, e.start) if not e.allday else e.start_date,
            'end': fields.Datetime.context_timestamp(
                self, e.stop) if not e.allday else e.stop_date,
            'allDay': e.allday
        }, event_ids))

        # Calcul des activités
        activity_ids = self.env['mail.activity'].sudo().search([
            ['user_id', '=', self.env.user.id]])
        activity_today_list = activity_ids.filtered(
            lambda a: a.date_deadline == Date.today())
        activity_today = list(map(lambda a: {
            'date_deadline': a.date_deadline,
            'activity_type_id': a.activity_type_id.name,
            'summary': a.summary,
            'partner': self.env[a.res_model].browse(a.res_id).name,
        }, activity_today_list))

        activity_week = activity_ids.filtered(
            lambda a: a.date_deadline > Date.today() and a.date_deadline <= (
                Date.today() + timedelta(days=7)))
        activity_overdue = activity_ids.filtered(
            lambda a: a.date_deadline < Date.today())

        # Calcul du temps imputé sur la semaine
        timesheet = self.env['account.analytic.line'].sudo().search([
            ['user_id', '=', self.env.user.id],
            ['date', '>=', week_start],
            ['date', '<=', week_end]])
        weektime = float_round(sum(timesheet.mapped('unit_amount')),
                               precision_digits=2)

        # Calcul du temps imputé sur le mois précédent
        timesheet_month = self.env['account.analytic.line'].sudo().search([
            ['user_id', '=', self.env.user.id],
            ['date', '>=', month_start],
            ['date', '<=', month_end]])
        last_monthtime = float_round(
            sum(timesheet_month.mapped('unit_amount')),
            precision_digits=2)

        # Récupération des Menus,
        # Id des Menus et Actions correspondantes
        list_links = []

        # Menu mes organismes
        menu_contact = self.env['ir.model.data'].sudo().search([
            ('name', '=', 'menu_contacts')])

        # Action mes coop suivies
        menu_ref = self.env['ir.model.data'].sudo().search([
            ('name', '=', 'home_scop_cooperative_suiv_act')])
        link_href = "#menu_id=" \
            + str(menu_contact.res_id) + "&action=" + str(menu_ref.res_id)

        list_links.append({'name': "Organismes suivis", 'href': link_href})

        # Action mes projets
        menu_ref = self.env['ir.model.data'].sudo().search([
            ('name', '=', 'home_scop_my_prospect_act')])
        link_href = "#menu_id=" \
            + str(menu_contact.res_id) + "&action=" + str(menu_ref.res_id)

        list_links.append({'name': "Mes projets", 'href': link_href})

        # Action mes révisions
        menu_ref = self.env['ir.model.data'].sudo().search([
            ('name', '=', 'home_scop_cooperative_rev_act')])
        link_href = "#menu_id=" \
            + str(menu_contact.res_id) + "&action=" + str(menu_ref.res_id)

        list_links.append({'name': "Les révisions", 'href': link_href})

        # Action mon suivi du temps
        menu_ref = self.env['ir.model.data'].sudo().search([
            ('name', '=', 'act_hr_timesheet_line')])

        mon_suivi = "#menu_id=" \
            + str(menu_contact.res_id) + "&action=" + str(menu_ref.res_id)

        return {
            'events': events,
            'weektime': weektime,
            'last_monthtime': last_monthtime,
            'activity_today': activity_today,
            'activity_week': len(activity_week),
            'activity_overdue': len(activity_overdue),
            'list_links': list_links,
            'mon_suivi': mon_suivi,
        }
