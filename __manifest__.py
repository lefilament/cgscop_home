{
    'name': "CG Scop - Home",
    'summary': "Page d'accueil CG Scop",
    'author': "LE FILAMENT",
    'category': 'dashboard',
    'website': "http://www.le-filament.com",
    'version': '10.0.1',
    'license': 'AGPL-3',
    'depends': [
        'cgscop_partner',
        'cgscop_resource',
        'hr_timesheet'
    ],
    'data': [
        'views/assets.xml',
        'views/cgscop_home.xml',
        'views/cgscop_partner.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
}
