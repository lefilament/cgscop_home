 // © 2019 Le Filament (<http://www.le-filament.com>)
 // License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

odoo.define('cgscop_home.home', function (require) {
	"use strict";

	var core = require('web.core');
	var session = require('web.session');
	var AbstractAction = require('web.AbstractAction');
	var QWeb = core.qweb;

	var CgScopHome = AbstractAction.extend({
	    template: 'CgScopHome',

	    events: {
	    	'click #my-calendar': function(e) {
	        	e.preventDefault();
	        	this.do_action('calendar.action_calendar_event');
		    },
	        'click #my-activities': function(e) {
	        	e.preventDefault();
	        	this.do_action('cgscop_home.home_scop_activity_act');
		    },
	    },

	    willStart: function() {
	        var deferred = new jQuery.Deferred();
	        var self = this;
	        this.values = {};

	        this._rpc({
				    model: 'cgscop.home',
				    method: 'get_home_values',
				    args: [],
				})
                .then(function(results) {
                    self.values = results;
                    deferred.resolve();
                });
	        return jQuery.when(this._super.apply(this, arguments),deferred);
	    },

	    start: function() {
	    	this.calendar = this.render_calendar();
	    	this.calendar.refetchEvents();
	    },
	    render_calendar: function() {
	    	self = this
	    	var calendarEl = this.$el.find('#calendar')[0];
	        var calendar = new FullCalendar.Calendar(calendarEl, {
	        	themeSystem: 'bootstrap',
				plugins: [ 'bootstrap', 'dayGrid', 'timeGrid', 'list' ],
				defaultView: 'timeGridWeek',
				minTime: '07:00:00',
				maxTime: '21:00:00',
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'timeGridWeek,timeGridDay,dayGridMonth,listWeek'
				},
				events: self.values.events,
				locale: 'fr',
				buttonIcons: true,
				weekNumbers: true,
				navLinks: true,
				timeZone: 'Europe/Paris',
				businessHours: {
					daysOfWeek: [ 1, 2, 3, 4, 5 ],
					startTime: '09:00',
					endTime: '18:00',
				},
				height: 'parent',
	        });
	        setTimeout(function() {calendar.render();}, 100);
	        return calendar;
	    },

	});

	core.action_registry.add('cgscop_home.home', CgScopHome);


});

